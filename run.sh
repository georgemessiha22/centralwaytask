#!/usr/bin/env bash
pip install virtualenv
virtualenv env
source activate env/bin/activate
pip install -r requirments.txt
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py runserver