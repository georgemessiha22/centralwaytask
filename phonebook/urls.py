from django.conf.urls import url
from rest_framework.authtoken import views as auth_views

from .views import CreateUserView, ContactView, add_phone

urlpatterns = [
	url(r'^user', auth_views.obtain_auth_token),
	url(r'^register', CreateUserView.as_view()),
	url(r'^contacts/(?P<contacts_id>[0-9]+)/entries', add_phone),
	url(r'^contacts/(?P<contacts_id>[0-9]+)', ContactView.as_view()),
	url(r'^contacts', ContactView.as_view()),
	]
