from django.utils.six import text_type
from django.utils.translation import ugettext_lazy as _
from rest_framework import HTTP_HEADER_ENCODING, exceptions
from rest_framework.authentication import BaseAuthentication


def get_token_header(request):
	"""
	Return request's 'Authorization:' header, as a bytestring.

	Hide some test client ickyness where the header can be unicode.
	"""
	auth = request.META.get('HTTP_TOKEN', b'')
	if isinstance(auth, text_type):
		# Work around django test client oddness
		auth = auth.encode(HTTP_HEADER_ENCODING)
	return auth


class TokenAuthentication(BaseAuthentication):
	"""
	Simple token based authentication.

	Clients should authenticate by passing the token key in the "Token"
	HTTP header, prepended with the string "<Token>".  For example:

		Token: 401f7ac837da42b97f613d789819ff93537bee6a
	"""
	
	keyword = 'Token'
	model = None
	
	def get_model(self):
		if self.model is not None:
			return self.model
		from rest_framework.authtoken.models import Token
		return Token
	
	"""
	A custom token model may be used, but must have the following properties.

	* key -- The string identifying the token
	* user -- The user to which the token belongs
	"""
	
	def authenticate(self, request):
		auth = get_token_header(request)
		
		if not auth:
			return None
		try:
			token = auth.decode()
		except UnicodeError:
			msg = _('Invalid token header. Token string should not contain invalid characters.')
			raise exceptions.AuthenticationFailed(msg)
		
		return self.authenticate_credentials(token)
	
	def authenticate_credentials(self, key):
		model = self.get_model()
		try:
			token = model.objects.select_related('user').get(key=key)
		except model.DoesNotExist:
			raise exceptions.AuthenticationFailed(_('Invalid token.'))
		
		if not token.user.is_active:
			raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))
		
		return (token.user, token)
	
	def authenticate_header(self, request):
		return self.keyword
