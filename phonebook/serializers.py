from django.contrib.auth import get_user_model  # If used custom user model
from rest_framework import serializers

from .constants import FIRST_NAME, LAST_NAME, PHONE_NUMBER, ID, PHONES
from .models import Phone, Contact

UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):
	password = serializers.CharField(write_only=True)
	
	def create(self, validated_data):
		user = UserModel.objects.create(
				username=validated_data['username']
				)
		user.set_password(validated_data['password'])
		user.save()
		
		return user
	
	class Meta:
		model = UserModel
		fields = '__all__'


class ContactRequestSerializer(serializers.Serializer):
	first_name = serializers.CharField(required=True, max_length=255)
	last_name = serializers.CharField(required=False, allow_blank=True, max_length=255)
	id = serializers.CharField(required=False)
	

class AddPhoneRequestSerializer(serializers.Serializer):
	phone = serializers.CharField(required=True, max_length=20)