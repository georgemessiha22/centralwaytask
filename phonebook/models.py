from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
	if created:
		Token.objects.create(user=instance)


class Contact(models.Model):
	first_name = models.CharField(verbose_name="Firstname", max_length=255, unique=False, blank=False, null=False)
	last_name = models.CharField(verbose_name="Lastname", max_length=255, null=True, blank=True)
	creation_date = models.DateTimeField(auto_now_add=True)
	user_id = models.ForeignKey(User, on_delete=models.CASCADE)
	

class Phone(models.Model):
	phone_number = models.CharField(verbose_name="Phone", max_length=20, unique=False)
	creation_date = models.DateTimeField(auto_now_add=True)
	contact_id = models.ForeignKey(Contact, related_name='phones', on_delete=models.CASCADE)
	
	def __unicode__(self):
		return '%s' % (self.phone_number)
